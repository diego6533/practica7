package mx.unitec.moviles.practica7

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import mx.unitec.moviles.practica7.model.Contact
import mx.unitec.moviles.practica7.viewmodel.ContactsViewModel
import mx.unitec.moviles.practica7.widget.ContactDialogFragment
import mx.unitec.moviles.practica7.widget.NoticeDialogListener

class MainActivity : AppCompatActivity(), NoticeDialogListener {

    private lateinit var contactsViewModel: ContactsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        contactsViewModel = ViewModelProvider(this).get(ContactsViewModel::class.java)

    }

    fun addContact(view: View) {
        val addDialog = ContactDialogFragment()
        addDialog.show(supportFragmentManager, "contacto")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDialogPositiveClick(dialog: DialogFragment, contact: Contact) {
        contactsViewModel.saveContact(contact)
        Snackbar.make(root_layout, "Se agregó ${contact.name}", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {
        //Implementar
    }
}